# Cluster Execution Role
resource "aws_iam_role" "this" {
  name               = "${var.app_name}-ecs-assume-ec2-role"
  assume_role_policy = file("${path.module}/policies/assume_role_ec2.json")
}

# AmazonEC2ContainerServiceforEC2Role
resource "aws_iam_role_policy_attachment" "ec2_role" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
  role       = aws_iam_role.this.id
}

# Rex Ray Policy - This allow the mount of volumes on EC2 Tasks
resource "aws_iam_role_policy" "rex_ray" {
  name   = "${var.app_name}-role-policy"
  policy = file("${path.module}/policies/rexray.json")
  role   = aws_iam_role.this.id
}

resource "aws_iam_instance_profile" "this" {
  name = "${var.app_name}-ec2-instance-profile"
  role = aws_iam_role.this.name
}
