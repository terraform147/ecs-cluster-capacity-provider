# ECS Cluster with Capacity Provider

This Terraform script set's up an environment
of a ECS Cluster where you can use FARGATE and
EC2 launch types using a Capacity Provider for
the last one. 
It uses an Amazon Linux AMI optimized for ECS use.
On `variables.tf` we have saved the AMI ID from 
the images available on `us-east-1` and `us-east-2`.
If you are launching this infrastructure on another
region please check AWS Docs. [1]

>> Please be careful on the naming of the Capacity Provider:
Until this day(March/2020) you are not able to delete a CP, 
only create and update. So if you mess up with the infrastructure
and try to set up again, you may have some problems.

## Security Group Configuration
Note that in `security_group.tf`, the only setup existent is the `egress`. The ports
that your ECS Task may need to use to work need to be opened on this security group.
You can add Security Group Rules referencing this SG ID, on other modules that will need
to use this Capacity Provider. [2]

## Mount a Volume
The EC2 Auto Scaling Group already has a policy using RexRay to mount EBS modules
on the ECS Task that may need that. [3]

## Setup module

```
module "cluster" {
    source = "git@gitlab.com:terraform147/ecs-cluster-capacity-provider.git"
    app_name = ""
    instance_type = ""
    min_size = ""
    max_size = ""
}
```

## Links
[1] - https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html

[2] - https://www.terraform.io/docs/providers/aws/r/security_group_rule.html

[3] - https://rexray.readthedocs.io/en/stable/user-guide/schedulers/docker/plug-ins/

With this ECS Cluster you are able to run a Fargate and EC2 cluster without any problems.