variable "app_name" {
  description = "Application name with environment"
  type        = string
}

variable "instance_type" {
  default = "m5.large"
}

variable "min_size" {
  description = "ASG min instances"
  default     = 1
}

variable "max_size" {
  description = "ASG max instances"
  default     = 1
}

# ECS optimized Ami from AWS
variable "ohio_ami" {
  default = "ami-0c0415cdff14e2a4a"
}

variable "virginia_ami" {
  default = "ami-098616968d61e549e"
}
