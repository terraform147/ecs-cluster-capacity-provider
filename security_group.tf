resource "aws_security_group" "this" {
  name   = "${var.app_name}-launch-config-sg"
  vpc_id = data.aws_vpc.selected.id

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "internet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "${var.app_name}-sg"
    Product = var.app_name
  }
}
