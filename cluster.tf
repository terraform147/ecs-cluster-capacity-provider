resource "aws_ecs_cluster" "this" {
  name = "${var.app_name}-ecs-cluster"

  capacity_providers = ["FARGATE", aws_ecs_capacity_provider.this.name]
  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.this.name
    weight            = 1
    base              = 1
  }

  tags = {
    Product = var.app_name
  }

  depends_on = [aws_ecs_capacity_provider.this]
}
