# Setup launch configuration for EC2
data "template_file" "user_data" {
  template = file("${path.module}/scripts/user_data.sh")
  vars = {
    cluster_name = "${var.app_name}-ecs-cluster"
    region       = data.aws_region.selected.name
  }
}

resource "aws_launch_configuration" "this" {
  name_prefix          = "${var.app_name}-default-launch-config-"
  iam_instance_profile = aws_iam_instance_profile.this.name
  image_id             = data.aws_region.selected.name == "us-east-1" ? var.virginia_ami : data.aws_region.selected.name == "us-east-2" ? var.ohio_ami : ""
  instance_type        = var.instance_type
  key_name             = var.app_name
  security_groups      = [aws_security_group.this.id]
  user_data            = data.template_file.user_data.rendered

  lifecycle {
    create_before_destroy = true
  }
}

# Setup the auto scaling group
resource "aws_placement_group" "this" {
  name     = "${var.app_name}-default-placement-group"
  strategy = "cluster"
}

resource "aws_autoscaling_group" "this" {
  name                      = "${var.app_name}-default-autoscaling-group"
  desired_capacity          = var.min_size
  health_check_type         = "EC2"
  health_check_grace_period = 300
  launch_configuration      = aws_launch_configuration.this.name
  max_size                  = var.max_size
  min_size                  = var.min_size
  placement_group           = aws_placement_group.this.id
  protect_from_scale_in     = true
  vpc_zone_identifier       = tolist(data.aws_subnet_ids.private.ids)
  tag {
    key                 = "Product"
    value               = var.app_name
    propagate_at_launch = true
  }
  tag {
    key                 = "Name"
    value               = "${var.app_name}-ecs-instance"
    propagate_at_launch = true
  }
  termination_policies = ["NewestInstance"]

  lifecycle {
    create_before_destroy = true
    ignore_changes = [desired_capacity]
  }
}
