# Cluster Execution Role
resource "aws_iam_role" "task" {
  name               = "${var.app_name}-ecs-task-role"
  assume_role_policy = file("${path.module}/policies/assume_role_task.json")
}

# AmazonECSTaskExecutionRolePolicy
resource "aws_iam_role_policy_attachment" "this" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.task.id
}
